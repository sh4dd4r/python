import random

class Player(object):
    def __init__(self, name):
        self.name = name
    move = ''
    score = 0

class Game(object):
    human = Player('Human')
    opponent = Player('Opponent')
    valid_choices = ['rock', 'paper', 'scissors']

    def print_result(self, winner):
        if winner != 0:
            print "%s is the winner!" % winner.name
            print "Current score is:"
            print "Human: " + str(self.human.score)
            print "opponent: " + str(self.opponent.score)
        else:
            print 'Draw'
            print "Current score is:"
            print "Human: " + str(self.human.score)
            print "opponent: " + str(self.opponent.score)

    def player_decision(self):
        choice = raw_input('Choose your move (rock / paper / scissors): ')
        print ''
        if choice in self.valid_choices:
            self.human.move = choice
        else:
            print 'Invalid choice'
            self.player_decision()

    def opponent_decision(self):
        self.opponent.move = random.choice(self.valid_choices)

    def decision_phase(self):
        print ''
        self.player_decision()
        print 'Human chooses %s' % self.human.move
        self.opponent_decision()
        print 'Opponent chooses %s' % self.opponent.move

    def game_rules(self, move1, move2):
        if move1 == 'rock' and move2 == 'rock':
            return 'draw'
        elif move1 == 'rock' and move2 == 'paper':
            return 'lose'
        elif move1 == 'rock' and move2 == 'scissors':
            return 'win'
        if move1 == 'paper' and move2 == 'rock':
            return 'win'
        elif move1 == 'paper' and move2 == 'paper':
            return 'draw'
        elif move1 == 'paper' and move2 == 'scissors':
            return 'lose'
        if move1 == 'scissors' and move2 == 'rock':
            return 'lose'
        elif move1 == 'scissors' and move2 == 'paper':
            return 'win'
        elif move1 == 'scissors' and move2 == 'scissors':
            return 'draw'

    def compare_phase(self, human, opponent):
        if self.game_rules(human.move, opponent.move) == 'win':
            return human
        elif self.game_rules(human.move, opponent.move) == 'lose':
            return opponent
        else:
            return 0

    def adjust_score(self, winner):
        winner.score += 1

    def run_game(self):
        while True:
            self.decision_phase()
            winner = self.compare_phase(self.human, self.opponent)
            if winner != 0:
                self.adjust_score(winner)
                self.print_result(winner)
            else:
                self.print_result(0)

game = Game()
game.run_game()


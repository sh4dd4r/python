class Plant(object):
    def __init__(self):
        self.age = 0

def input():
    global people
    global fruits
    people = int(raw_input('Enter a number of people: '))
    fruits = int(raw_input('Enter a number of fruits: '))

def plant_plants():
    global fruits
    plants.append(Plant())
    fruits -= 1

def time():
    global week
    for plant in plants:
        plant.age += 1
    week += 1

def harvest():
    global fruits
    for plant in plants:
        fruits += plant.age

people = 0
fruits = 0
plants = []
week = 1
input()
while fruits <= people:
    # plant all available fruits
    while fruits != 0:
        plant_plants()
    # advance time
    time()
    # harvest
    harvest()
print 'Result: ' + str(week) + ' weeks.'

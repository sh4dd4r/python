import random

def read_input():
    input = list(raw_input('Input (c,v,C,V): '))
    if check_input(input):
        return input
    else:
        print 'Invalid input.'
        read_input()

def check_input(input):
    valid = ['c', 'v', 'C', 'V']
    for letter in input:
        if letter not in valid:
            return False
    return True

def cons(upper = False):
    consonants = list('bcdfghjklmnpqrstvwxyz')
    if upper:
        return str(random.choice(consonants)).upper()
    else:
        return random.choice(consonants)

def vow(upper = False):
    vowels = list('aeiou')
    if upper:
        return str(random.choice(vowels)).upper()
    else:
        return random.choice(vowels)

def transform(input):
    output = []
    for letter in input:
        if letter in ['c', 'C']:
            if letter.islower():
                output.append(cons())
            else:
                output.append(cons(True))
        else:
            if letter.islower():
                output.append(vow())
            else:
                output.append(vow(True))
    print output
    output = ''.join(output)
    return output


input = read_input()
output = transform(input)
print output

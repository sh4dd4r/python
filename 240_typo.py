import random

def read_input():
    global text
    with open('240_typo_input', 'r') as input:
        for line in input:
            text = text + line.strip() + ' '

def scramble(word):
    w = list(word)
    first = w.pop(0)
    last = w.pop(-1)
    random.shuffle(w)
    result = str(first)
    result = result + ''.join(w)
    result = result + str(last)
    return result

text = ''
read_input()
words = text.split(' ')
result = []
for word in words:
    if len(word) > 2:
        result.append(scramble(word))
    else:
        result.append(word)
print ' '.join(result)

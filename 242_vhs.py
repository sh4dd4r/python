class Show(object):
    def __init__(self, start_time, end_time, name = ''):
        self.start_time = start_time
        self.end_time = end_time
        self.name = name

def read_input():
    with open('242_vhs_input', 'r') as input:
        for line in input:
            show = line.strip().split(' ', 2)
            if len(show) == 2:
                shows.append(Show(int(show[0]), int(show[1])))
            else:
                shows.append(Show(int(show[0]), int(show[1]), show[2]))

shows = []
read_input()
result = []

while shows:
    select = shows.pop(0)
    shows = [x for x in shows if select.end_time <= x.start_time]
    result.append(select)

print len(result)
for show in result:
    print show.start_time, show.end_time, show.name

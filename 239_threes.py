def read_input():
    global input
    input = int(raw_input('Input (integer larger than 1: '))
    if input <= 1:
        print 'Invalid input.'
        read_input()

def div(input):
    if input % 3 == 0:
        print input, '0'
        return input / 3
    elif (input + 1) % 3 == 0:
        print input, '1'
        return (input + 1) / 3
    else:
        print input, '-1'
        return (input - 1) / 3

def loop():
    global input
    while input != 1:
        input = div(input)

input = 0
read_input()
loop()
print input
